class = require("pl.class")
pretty = require("pl.pretty")
tablex = require("pl.tablex")

Set = require("pl.Set")
List = require("pl.List")

Rect = require("lib.Rect")
Tile = require("lib.Tile")
Group = require("lib.TileGroup")

local ScreenManager = require('lib/screens/ScreenManager');

Player1 = require('src/screens/Player1');
Player2 = require('src/screens/Player2');


local Map = class.Map(class.properties)
local screen_w = 0
local screen_h = 0
local mousex = -1
local mousey = -1
local mgid = -1
local camx
local camy

function Map:_init(...)
    self.map = nil
end

function Map:load(name)
    self.map = require(name)
    self.gfx_tiles = {}
    self.tiles = {}
    self.groups = {}
    self.player_groups = {[1]={}, [2]={}, [3]={}, [4]={}}

    -- load sprites
    for i, tileset in ipairs(self.map.tilesets) do
        local raw = love.image.newImageData(tileset.image)
        local first_gid = tileset.firstgid
        local w,h = tileset.imagewidth, tileset.imageheight
        local gid = first_gid
        print("th " .. tileset.tileheight .. " tw " .. tileset.tilewidth)
        print("w " .. w .. " h " .. h)
        print("y from 0 .. ".. h/tileset.tileheight)
        print("x from 0 .. ".. w/tileset.tilewidth)
        print("gid " .. gid .. " first gid " .. first_gid)

        for y=0, math.floor(h/tileset.tileheight)-1 do
        for x=0, math.floor(w/tileset.tilewidth)-1 do
            local sprite = love.image.newImageData(tileset.tilewidth, tileset.tileheight)
            sprite:paste(raw, 0, 0, x*tileset.tilewidth, y*tileset.tileheight, tileset.tilewidth, tileset.tileheight)
            self.gfx_tiles[gid] = love.graphics.newImage(sprite)
            gid = gid + 1
        end
        end
    end
    -- todo: we should save/ set those tiles in the map file
    self.player_colours = {14,15,1,2}
    self:update_screen()
    self:random()
end

function Map:update_screen()
    screen_w = love.graphics.getWidth()
    screen_h = love.graphics.getHeight()
end

function Map:random()
    self.tiles = {}
    self.groups = {}
    self.player_groups = {[1]={}, [2]={}, [3]={}, [4]={}}
    for y=0, self.map.height do
    for x=0, self.map.width do
        local pos = {x, y}
        local tile_id = self:pos_to_id(pos)
        if tablex.find(self.player_colours, self:get_tile(pos)) then
            -- this is already a player color
        else
            self:set_tile(pos, math.random(16,26))
        end
        self.tiles[tile_id] = Tile(tile_id, pos)
        self.groups[tile_id] = Group(tile_id)
    end
    end
    self:expand_groups()
    self:find_player_groups()
    self.player_active = 1
end

function Map:get_group_color(group_id)
    local group = self.groups[group_id]
    return self:get_tile(tablex.keys(group.members)[1])
end

function Map:find_player_groups()
    -- in need to use pairs because ipairs will not iterate over all groups in some cases :/
    for group_id in pairs(self.groups) do
        local tile_gfx_id = self:get_group_color(group_id)
        -- print(group_id .. " " .. tostring(tile_gfx_id))
        local player_number = tablex.find(self.player_colours, tile_gfx_id)
        if player_number then
            self.player_groups[player_number][group_id] = true
        end
    end
end

function Map:expand_groups()
    local visited = {}
    for tile_id, tile in ipairs(self.tiles) do
        visited[tile_id] = true
        local tile_gfx_id = self:get_tile(tile.pos)
        for n in pairs(self:get_tile_neighbours(tile.pos)) do
            local tile_n = self.tiles[n]
            local tile_n_gfx_id = self:get_tile(tile_n.pos)
            if visited[n] ~= true then
                -- add tile neighbours
                tile:add_neighbour(n)
                tile_n:add_neighbour(tile_id)
                -- add group neighbours
                self.groups[tile_n.group_id]:add_neighbour(tile.group_id)
                self.groups[tile.group_id]:add_neighbour(tile_n.group_id)

                if tile_gfx_id == tile_n_gfx_id and tile.group_id ~= tile_n.group_id then
                    if self.groups[tile_n.group_id] == nil then
                        ---
                        -- This shit should never happen
                        ---
                        error("fuuuu, why is the group missing")
                    elseif self.groups[tile.group_id] == nil then
                        ---
                        -- This shit should never happen
                        ---
                        error("fuuuu, why is the group missing")
                    else
                        self:merge_groups(tile.group_id, tile_n.group_id)
                    end
                end
            end
        end
    end
end

function Map:merge_groups(group_id_a, group_id_b, change_colour)
    -- better be safe than sorry
    if group_id_a == group_id_b then
        print("fuuuuuuu")
        return
    end
    local group_a = self.groups[group_id_a]
    local group_b = self.groups[group_id_b]

    local group_a_color = self:get_group_color(group_id_a)

    for tile_id in pairs(group_b.members) do
        group_a:add_member(tile_id)
        self.tiles[tile_id].group_id = group_id_a
        if change_colour then self:set_tile(tile_id, group_a_color) end
    end


    for neighbour_id in pairs(group_b.neighbours) do
        -- group a has new neighbours
        if group_a.members[neighbour_id] == nil then
            group_a:add_neighbour(neighbour_id)
        end

        -- all neighbours of group b should be neighbours of group a now
        self.groups[neighbour_id]:del_neighbour(group_id_b)
        self.groups[neighbour_id]:add_neighbour(group_id_a)
    end
    self.groups[group_id_b] = nil
end

function Map:overpower(color_id)
    if tablex.find(self.player_colours, color_id) then return false end
    print("player_active " .. self.player_active)
    pretty.dump(tablex.keys(self.player_groups[self.player_active]))
    -- pretty.dump(self.player_groups)
    local success = false
    for group_id in pairs(self.player_groups[self.player_active]) do
        local new_friends = {}
        local group = self.groups[group_id]
        -- pretty.dump(group)
        for n_id in pairs(group.neighbours) do
            -- print(n_id .. " " ..tostring(self:get_group_color(n_id)))
            if self:get_group_color(n_id) == color_id then
                new_friends[n_id] = true
                success = true
            end
        end
        -- TODO: to fully support multiple start groups we need to test if we might overpower too much in some cases
        for n_id in pairs(new_friends) do
            self:merge_groups(group_id, n_id, true)
        end
    end

    return success
end

function Map:eliminate_enclosed_areas()
    -- TODO: to fully support multiple start groups, we need to set the rules
    local black_id = nil
    local white_id = nil
    local possible_black = Set()
    for group_id in pairs(self.player_groups[1]) do
        black_id = group_id
        for n_group_id in pairs(self:get_reachable_groups(group_id)) do
            possible_black[n_group_id] = true
        end
    end
    -- if true then return end
    local possible_white = Set()
    for group_id in pairs(self.player_groups[2]) do
        white_id = group_id
        for n_group_id in pairs(self:get_reachable_groups(group_id)) do
            possible_white[n_group_id] = true
        end
    end

    local only_black = possible_black - possible_white
    local only_white = possible_white - possible_black

    -- print("black foo")
    -- pretty.dump(only_black)
    for group_id in pairs(only_black) do
        self:merge_groups(black_id, group_id, true)
    end
    -- print("white foo")
    -- pretty.dump(only_white)
    for group_id in pairs(only_white) do
        self:merge_groups(white_id, group_id, true)
    end
end

function Map:get_reachable_groups(group_id)
    local reachable = Set()
    local visited = Set()

    local group = self.groups[group_id]
    local todo = tablex.copy(group.neighbours)
    while tablex.size(todo) > 0 do
        -- Why the fuck does #todo not work???
        group_id = Set.pop(todo)

        visited[group_id] = true
        group = self.groups[group_id]
        if tablex.find(self.player_colours, self:get_group_color(group_id)) then
            -- continue
        else
            reachable[group_id] = true
            local n_id
            for n_id in pairs(group.neighbours) do
                if visited[n_id] then
                    -- nothing
                else
                    Set.add(todo, n_id)
                end
            end
        end
    end
    return reachable
end

function Map:set_tile(pos_or_id, value)
    local id
    if type(pos_or_id) == "number" then
        id = pos_or_id
    else
        -- if self:is_position_valid(pos_or_id) == false then return false end
        id = self:pos_to_id(pos_or_id)
    end
    if self:is_id_valid(id) == false then return false end
    self.map.layers[1].data[id] = value
    return true
end
function Map:get_tile(pos_or_id)
    local id
    if type(pos_or_id) == "number" then
        id = pos_or_id
    else
        id = self:pos_to_id(pos_or_id)
    end
    return self.map.layers[1].data[id]
end

function Map:pos_to_id(pos)
    if self:is_position_valid(pos) then
        return 1 + pos[1] + self.map.width * pos[2]
    end
    return -1
end

function Map:is_position_valid(pos)
    if pos[1] < 0 or pos[1] >= self.map.width or pos[2] < 0 or pos[2] >= self.map.height then
        return false
    end
    return true
end

function Map:is_id_valid(id)
    if id <= 0 or id > self.map.width * self.map.height then
        return false
    end
    return true
end

function Map:get_tile_neighbours(pos)
    local neighbours = {}
    local parity = pos[1] % 2 + 1
    local directions = {
       { {1,  0}, {1, -1}, { 0, -1}, {-1, -1}, {-1,  0}, { 0, 1} },
       { {1, 1}, {1,  0}, { 0, -1}, {-1,  0}, {-1, 1}, { 0, 1} }
    }

    -- local dir = directions[parity][direction]
    for _, dir in ipairs(directions[parity]) do
        local new_pos = {(pos[1] + dir[1]), (pos[2] + dir[2])}
        if self:is_position_valid(new_pos) then
            -- table.insert(neighbours, self:pos_to_id(new_pos))
            neighbours[self:pos_to_id(new_pos)] = true
        end
    end
    -- pretty.dump(neighbours)
    return neighbours
end

function Map:advance_player()
    self:eliminate_enclosed_areas()
    if self.player_active == 2 then
        self.player_active = 1
        ScreenManager.push(Player1.new());

    else
        self.player_active = self.player_active + 1
        -- TODO support for more than 2 players
        ScreenManager.push(Player2.new());
    end
end
function Map:mousemoved(x, y, dx, dy)
    worldx = x + camx - screen_w / 2
    worldy = y + camy - screen_h / 2

    local draw_tile_width = (self.map.tilewidth - self.map.hexsidelength / 2 + 1)
    local draw_tile_height = (self.map.tileheight + 1)

    -- column, column_reminder = divmod(x,  draw_wid)
    -- delta = ODD_COL_DISTANCE if column % 2 == 1 else EVEN_COL_DISTANCE
    column = math.floor(worldx / draw_tile_width)
    local delta = 0
    if column % 2 == 1 then
        delta = draw_tile_height / 2
    end
    -- row, row_remainder = divmod((y - delta), ROW_HEIGHT)
    row = math.floor((worldy - delta) / draw_tile_height)
    -- if (column_reminder < COL_OVERLAP) then
    --     -- # we need to check better
    --     normalized_y = (row_remainder - HALF_HEIGHT) / HALF_HEIGHT
    --     normalized_x = column_reminder / COL_OVERLAP
    --     qx, qy, qz = self.cube
    --     if normalized_x >= abs(normalized_y) then
    --         -- # we are safe
    --         -- pass
    --     elseif -normalized_y > normalized_x then
    --         self.cube = qx - 1, qy + 1, qz
    --     elseif normalized_y > normalized_x then
    --         self.cube = qx - 1, qy, qz + 1
    -- body
    mousex, mousey = column, row
    mgid = self:get_tile({mousex, mousey})
end

function Map:mousepressed(x, y, button)
    local mpos = {mousex, mousey}
    local mkey = self:pos_to_id(mpos)
    if button == "l" then
        -- change color of hextile under the mouse cursor
        if tablex.find(self.player_colours, self:get_tile(mpos)) then
            return
        end

        if tablex.find({1, 2}, self.player_active) then
            -- if self:set_tile(mpos, self.player_active) then self:advance_player() end
            if self:overpower(mgid) then self:advance_player() end
        end
    elseif button == "r" then
        print(" position x ".. mousex .. " y " .. mousey .. " is valid ".. tostring(self:is_position_valid(mpos)))
        pretty.dump(self.tiles[mkey])
        print("group")
        pretty.dump(self.groups[self.tiles[mkey].group_id])
        print("reach")
        pretty.dump(self:get_reachable_groups(self.tiles[mkey].group_id))
        print("------------------------")
    end
end

function Map:draw(gCamX,gCamY)
    -- draw_map_hex_x_odd(gCamX,gCamY)
    camx, camy = gCamX, gCamY
    local draw_tile_width = (self.map.tilewidth - self.map.hexsidelength / 2 + 1)
    local draw_tile_height = (self.map.tileheight + 1)
    local gid = 1

    -- calculate the minimal and maximal index that is partly visible
    local minx,maxx = math.floor((camx-screen_w/2)/draw_tile_width) -1 ,math.ceil((camx+screen_w/2)/draw_tile_width)
    local miny,maxy = math.floor((camy-screen_h/2)/draw_tile_height) -1 ,math.ceil((camy+screen_h/2)/draw_tile_height)
    local x_list = {}
    for x=math.max(minx, 0), math.min(maxx, self.map.width -1) do
        table.insert(x_list, x)
    end

    local mid = self:pos_to_id({mousex, mousey})
    local mouse_tile = self.tiles[self:pos_to_id({mousex, mousey})]
    

    local function sort(a,b)
        -- sort the list, but prefer even numbers
        if (a % 2 == b % 2 ) then
            if (a < b) then
                return true
            end
            return false
        end
        if (a % 2 == 0) then
            return true
        end
        return false
    end
    table.sort(x_list, sort)

    for y=math.max(miny, 0), math.min(maxy, self.map.height -1) do
    for _,x in ipairs(x_list) do
        gid = self:get_tile({x, y})
        if (x < 10 and y < 10 and onetime) then
            -- print (x .. " " .. y .. ":  " ..  gid)
        end
        if gid then
            local gfx = self.gfx_tiles[gid]
            local sx = x * draw_tile_width - camx + screen_w/2
            -- todo get tile image height from map
            -- 60 = tilesset.tileheight
            local sy = y * draw_tile_height - camy + screen_h/2 - (60 -self.map.tileheight)
            if x % 2 == 1 then
                sy = sy + math.floor(self.map.tileheight / 2)
            end
            -- if (x == mousex and y == mousey) then
            if mid == self:pos_to_id({x, y}) then
                sy = sy + 4
            else
                local draw_tile = self.tiles[self:pos_to_id({x,y})]
                if draw_tile and mouse_tile and draw_tile.group_id == mouse_tile.group_id then
                    sy = sy + 4
                end
                    
                -- for n in pairs(self:get_tile_neighbours({mousex, mousey})) do
                --     if n[1] == x and n[2] == y then
                --         sy = sy + 2
                --     end
                -- end
            end
            love.graphics.draw(gfx,sx,sy)
        end
    end
    end
    -- end 
    if info then
        love.graphics.print('arrow-keys=scroll, space=next map', 50, 50)
        love.graphics.print('camx ' .. camx .. ' | camy ' .. camy, 50, 60)
        love.graphics.print('mousex ' .. mousex .. ' | mousey ' .. mousey .. " | id " .. mid .. " | col " .. tostring(self:get_tile(mid)), 50, 70)
        love.graphics.print('sw ' .. screen_w .. ' | sh ' .. screen_h, screen_w-300, screen_h-13)
    end
end

function Map:__tostring()
    return "Map(" .. self.x .. "," .. self.y .. ")"
end
return Map
