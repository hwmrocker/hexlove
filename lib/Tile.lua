class = require("pl.class")
Set = require("pl.Set")
Point = require("lib.Point")

local Tile = class.Tile(class.properties)

function Tile:_init(id, pos)
    self.id = id
    self.pos = Point(pos)
    self.group_id = id
    self.neighbours = Set()
end

function Tile:add_neighbour(neighbour)
    self.neighbours[neighbour] = true
end
function Tile:del_neighbour(neighbour)
    self.neighbours[neighbour] = nil
end

function Tile:add_neighbours(neighbours)
    for _,n in ipairs(neighbours) do
        self.neighbours[n] = true
    end
end
function Tile:del_neighbours(neighbours)
    for _,n in ipairs(neighbours) do
        self.neighbours[n] = nil
    end
end

function Tile:__tostring()
    return "Tile(" .. self.id .. ")"
end
return Tile
