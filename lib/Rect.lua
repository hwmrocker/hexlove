class = require("pl.class")
local Point = require("lib.Point")

local Rect = class.Rect(class.properties)

function Rect:_init(...)
    local nargs = select('#',...)
    local extra = {...}
    local x = 0
    local y = 0
    local w = 0
    local h = 0

    if nargs == 4 then
        -- Rect(x,y,w,h) was used
        x = extra[1]
        y = extra[2]
        w = extra[3]
        h = extra[4]
    elseif nargs == 2 then
        if type(a) == "number" then
            -- Rect(w,h) was used
            w = extra[1]
            h = extra[2]
        else
            -- Rect({x,y},{w,h}) was used
            x = extra[1][1]
            y = extra[1][2]
            w = extra[2][1]
            h = extra[2][2]
        end
    elseif nargs == 1 then
        -- Rect({w,h}) was used
        w = extra[1][1]
        h = extra[1][2]
    elseif nargs == 0 then
        -- nothing
    else
        error("wrong number of arguments, use Rect(x,y,w,h), Rect({x,y}, {w,h}), Rect(w,h), or Rect({w,h})")
    end

    self.x = x
    self.y = y
    self.w = w
    self.h = h
end

-- value based setter and getter functions
function Rect:set_top(value)
    self.y = value
end
function Rect:get_top()
    return self.y
end

function Rect:set_left(value)
    self.x = value
end
function Rect:get_left()
    return self.x
end

function Rect:set_bottom(value)
    self.y = value - self.h
end
function Rect:get_bottom()
    return self.y + self.h
end

function Rect:set_right(value)
    self.x = value - self.w
end
function Rect:get_right()
    return self.x + self.w
end

function Rect:set_center_x(value)
    self.x = value - math.floor(self.w / 2)
end
function Rect:get_center_x()
    return self.x + math.floor(self.w / 2)
end

function Rect:set_center_y(value)
    self.y = value - math.floor(self.h / 2)
end
function Rect:get_center_y()
    return self.y + math.floor(self.h / 2)
end

-- point based setter and getter function
-- Points will always ask first for the x-dimension
function Rect:set_topleft(point)
    self.left = point[1]
    self.top = point[2]
end
function Rect:get_topleft()
    return Point(self.left, self.top)
end

function Rect:set_bottomleft(point)
    self.left = point[1]
    self.bottom = point[2]
end
function Rect:get_bottomleft()
    return Point(self.left, self.bottom)
end

function Rect:set_topright(point)
    self.right = point[1]
    self.top = point[2]
end
function Rect:get_topright()
    return Point(self.right, self.top)
end

function Rect:set_bottomright(point)
    self.right = point[1]
    self.bottom = point[2]
end
function Rect:get_bottomright()
    return Point(self.right, self.bottom)
end


function Rect:set_topcenter(point)
    self.center_x = point[1]
    self.top = point[2]
end
function Rect:get_topcenter()
    return Point(self.center_x, self.top)
end

function Rect:set_leftcenter(point)
    self.left = point[1]
    self.center_y = point[2]
end
function Rect:get_leftcenter()
    return Point(self.left, self.center_y)
end

function Rect:set_bottomcenter(point)
    self.center_x = point[1]
    self.bottom = point[2]
end
function Rect:get_bottomcenter()
    return Point(self.center_x, self.bottom)
end

function Rect:set_rightcenter(point)
    self.right = point[1]
    self.center_y = point[2]
end
function Rect:get_rightcenter()
    return Point(self.right, self.center_y)
end

function Rect:set_size(point)
    self.w = point[1]
    self.h = point[2]
end
function Rect:get_size()
    return Point(self.w, self.h)
end

return Rect
