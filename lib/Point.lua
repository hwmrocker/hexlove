class = require("pl.class")
local Point = class.Point(class.properties)

function Point:_init(x, y)
    if y == nil then
        self.x = x[1]
        self.y = x[2]
    else
        self.x = x
        self.y = y
    end
end

function Point:set_1(point)
    self.x = point
end
function Point:get_1()
    return self.x
end

function Point:set_2(point)
    self.y = point
end
function Point:get_2()
    return self.y
end

function Point:__tostring()
    return "Point(" .. self.x .. "," .. self.y .. ")"
end
return Point
