class = require("pl.class")
Set = require("pl.Set")

local TileGroup = class.TileGroup(class.properties)

function TileGroup:_init(id)
    self.id = id
    self.members = Set()
    self.neighbours = Set()
    self:add_member(id)
end

function TileGroup:add_member(member)
    self.members[member] = true
end
function TileGroup:del_member(member)
    self.members[member] = nil
end

function TileGroup:add_neighbour(neighbour)
    if neighbour == id then return end
    self.neighbours[neighbour] = true
end
function TileGroup:del_neighbour(neighbour)
    self.neighbours[neighbour] = nil
end

function TileGroup:__tostring()
    return "TileGroup(" .. self.id .. ")"
end
return TileGroup
