local foo = "rect"
Rect = require(foo);
Point = require("point")
p = require("pl.pretty")

r = Rect(10,5,15,10)
print("size: " .. r.size:__tostring())
print("")
print("topleft: " .. r.topleft:__tostring())
print("bottomleft: " .. r.bottomleft:__tostring())

print("")
print("topright: " .. r.topright:__tostring())
print("bottomright: " .. r.bottomright:__tostring())

print("")
print("leftcenter: " .. r.leftcenter:__tostring())
print("rightcenter: " .. r.rightcenter:__tostring())

print("")
print("topcenter: " .. r.topcenter:__tostring())
print("bottomcenter: " .. r.bottomcenter:__tostring())

print("")
print("left: " .. r.left)
print("right: " .. r.right)
print("top: " .. r.top)
print("bottom: " .. r.bottom)

print("")
print("center_x: " .. r.center_x)
print("center_y: " .. r.center_y)

r.topleft = Point(4,5)
print("topleft: " .. r.topleft:__tostring())
print("size: " .. r.size:__tostring())
