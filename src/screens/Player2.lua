--===============================================================================--
--                                                                               --
-- Copyright (c) 2014 Robert Machmer                                             --
--                                                                               --
-- This software is provided 'as-is', without any express or implied             --
-- warranty. In no event will the authors be held liable for any damages         --
-- arising from the use of this software.                                        --
--                                                                               --
-- Permission is granted to anyone to use this software for any purpose,         --
-- including commercial applications, and to alter it and redistribute it        --
-- freely, subject to the following restrictions:                                --
--                                                                               --
--  1. The origin of this software must not be misrepresented; you must not      --
--      claim that you wrote the original software. If you use this software     --
--      in a product, an acknowledgment in the product documentation would be    --
--      appreciated but is not required.                                         --
--  2. Altered source versions must be plainly marked as such, and must not be   --
--      misrepresented as being the original software.                           --
--  3. This notice may not be removed or altered from any source distribution.   --
--                                                                               --
--===============================================================================--

local ScreenManager = require('lib/screens/ScreenManager');
local Screen = require('lib/screens/Screen');

local lg = love.graphics

-- ------------------------------------------------
-- Module
-- ------------------------------------------------

local Intro = {};

-- ------------------------------------------------
-- Constants
-- ------------------------------------------------

local DISPLAY_TIME = 3;

-- ------------------------------------------------
-- Constructor
-- ------------------------------------------------

function Intro.new()
    local self = Screen.new();

    function self:init()
    end

    local counter = 0;
    function self:draw_if_active()
        lg.setBackgroundColor(0xF0,0xF0,0xF0)
        lg.clear()
        -- old_font = lg.getFont()
        lg.setNewFont(40)
        lg.printf("Player 2", 0, 300, lg.getWidth(), "center") 
        lg.setNewFont()
        -- lg.set_font(old_font)
        if info then
            lg.print('time left ' .. DISPLAY_TIME - counter, 50, 80)
        end
    end

    function self:update(dt)
        counter = counter + dt;
        if counter > DISPLAY_TIME then
            ScreenManager.pop();
        end
    end

    function self:keypressed(key)
        ScreenManager.pop();
    end

    function self:mousepressed()
        ScreenManager.pop();
    end

    return self;
end

-- ------------------------------------------------
-- Return Module
-- ------------------------------------------------

return Intro;

--==================================================================================================
-- Created 03.11.14 - 18:03                                                                        =
--==================================================================================================
