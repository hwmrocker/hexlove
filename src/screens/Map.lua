--===============================================================================--
--                                                                               --
-- Copyright (c) 2014 Robert Machmer                                             --
--                                                                               --
-- This software is provided 'as-is', without any express or implied             --
-- warranty. In no event will the authors be held liable for any damages         --
-- arising from the use of this software.                                        --
--                                                                               --
-- Permission is granted to anyone to use this software for any purpose,         --
-- including commercial applications, and to alter it and redistribute it        --
-- freely, subject to the following restrictions:                                --
--                                                                               --
--  1. The origin of this software must not be misrepresented; you must not      --
--      claim that you wrote the original software. If you use this software     --
--      in a product, an acknowledgment in the product documentation would be    --
--      appreciated but is not required.                                         --
--  2. Altered source versions must be plainly marked as such, and must not be   --
--      misrepresented as being the original software.                           --
--  3. This notice may not be removed or altered from any source distribution.   --
--                                                                               --
--===============================================================================--

local ScreenManager = require('lib/screens/ScreenManager');
local Screen = require('lib/screens/Screen');

-- ------------------------------------------------
-- Module
-- ------------------------------------------------

local MapScreen = {};

-- ------------------------------------------------
-- Constructor
-- ------------------------------------------------

function MapScreen.new()
    local self = Screen.new();

    local gKeyPressed = {}
    local gCamX,gCamY = love.graphics.getWidth(), love.graphics.getHeight()
    local pretty = require("pl.pretty")
    local Rect = require("lib.Rect")
    local Map = require("lib.Map")

    local onetime = true
    local middle_mouse_pressed = false
    local foo = false
    local mousex, mousey = -1, -1
    local map = Map()
    local screen_w
    local screen_h
    local screen_rect = Rect()

    local index = 1;

    function self:init()
        map:load("maps/empty")
        screen_w = love.graphics.getWidth()
        screen_h = love.graphics.getHeight()
        screen_rect.size = {screen_w, screen_h}
    end

    function self:resize(w, h)
        screen_w = love.graphics.getWidth()
        screen_h = love.graphics.getHeight()
        screen_rect.size = {screen_w, screen_h}
        map:update_screen()
        gCamX,gCamY = screen_rect.center_x, screen_rect.center_y
    end

    function self:keyreleased( key )
        gKeyPressed[key] = nil
    end

    function self:keypressed( key, unicode ) 
        gKeyPressed[key] = true 
        if (key == "escape") then os.exit(0) end
        if (key == " ") then -- space = reset cam
            gCamX,gCamY =  screen_rect.center_x, screen_rect.center_y
            foo = not foo
        elseif (key == "r") then
            map:load("maps/empty")
        end
    end

    function self:mousemoved( x, y, dx, dy )
        if (middle_mouse_pressed) then
            gCamY = gCamY - dy
            gCamX = gCamX - dx
        end

        map:mousemoved(x, y, dx, dy)
    end

    function self:mousepressed(x, y, button)
        if (button == "m") then
            middle_mouse_pressed = true
        end
        map:mousepressed(x, y, button)
    end

    function self:mousereleased(x, y, button)
        if (button == "m") then
            middle_mouse_pressed = false
        end
    end

    function self:update( dt )
        local s = 500*dt
        -- Achtung Kopie, ausgliedern nach Map
        local draw_tile_width = (map.map.tilewidth - map.map.hexsidelength / 2 + 1)
        local draw_tile_height = (map.map.tileheight + 1)

        if (gKeyPressed.up) then gCamY = gCamY - s end
        if (gKeyPressed.down) then gCamY = gCamY + s end
        if (gKeyPressed.left) then gCamX = gCamX - s end
        if (gKeyPressed.right) then gCamX = gCamX + s end
        gCamX = math.min(math.max(gCamX, screen_w / 2), screen_w / -2 + map.map.width * draw_tile_width + map.map.tilewidth - draw_tile_width)
        gCamY = math.min(math.max(gCamY, screen_h / 2), screen_h / -2 + (map.map.height + 1) * draw_tile_height)
    end

    function self:draw()
        if map.player_active == 1 then
            love.graphics.setBackgroundColor(0x00,0x00,0x00)
        elseif map.player_active == 2 then
            love.graphics.setBackgroundColor(0xC0,0xC0,0xC0)
        else
            love.graphics.setBackgroundColor(0x80,0x80,0x80)
        end
        map:draw(gCamX,gCamY)
    end


    return self;
end

-- ------------------------------------------------
-- Return Module
-- ------------------------------------------------

return MapScreen;

--==================================================================================================
-- Created 03.11.14 - 18:03                                                                        =
--==================================================================================================
